$(function(){
    if (window.matchMedia("(orientation: portrait)").matches) {
        $('body').css('padding-top', '80px');
    }
    
    $('#selectModal').modal({'show': true, 'backdrop': 'static'}).modal('show');
});

var new_rows = [];
var row_index = 0;
var piece_class;
var incomplet_row_class;
var clicked = {'id': 0, 'img': ''};
var timer_started = false;
var interval_id;

function start(piece_count) {
    new_rows = (piece_count == 8) ? [1, 4, 6] : [1, 7, 11];
    piece_class = (piece_count == 8) ? 'col-4 col-sm-4 piece-image' : 'col-2 col-sm-2 piece-image';

    list = getListOrder(piece_count);
    list = list.sort(function(a, b){return Math.random() - 0.5});

    list.forEach(function(number, index){
        index++;
        row_index = new_rows.includes(index) ? row_index + 1 : row_index;
        var row = $('#row_'+row_index).length ? $('#row_'+row_index) : createRow(row_index);
        var piece = createPiece(index, piece_class);

        var image = piece.find('img');
        
        // linhas incompletas
        if (row_index == 2) {
            row.addClass('incomplete');
        }

        image.attr('src', 'img/back.png').attr('data-img', 'img/'+number+'.jpg').prop('id', 'img_'+index).addClass('unclicked');
        row.append(piece);
    });

    fixPiecesSize();

    var empty_piece = createPiece('empty', piece_class);
    empty_piece.find('img').remove();

    // inserindo espaços vazios
    $((piece_count == 8) ? '#piece_4' : '#piece_7, #piece_9').after(empty_piece)

    $('#selectModal').modal('hide');
}

function createRow(index) {
    var row = $('#row_template').clone().prop('id', 'row_'+index);
    $('#piece_container').append(row);

    return row;
}

function createPiece(index, piece_class) {
    var piece = $('#piece_template').clone().prop('id', 'piece_'+index).show();
    piece.addClass(piece_class);

    return piece;
}

function getListOrder(piece_count) {
    if (piece_count == 8) {
        list =  [];

        // pega 4 números aleatórios de 1 a 8
        do {
            var number = Math.floor((Math.random() * 7) + 1);
            if (!list.includes(number)) {
                list.push(number);
            }
        } while (list.length < 4);


        // duplica os valores
        list = list.concat(list);
    } else {
        list = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8];
    }
    
    return list;
}

function fixPiecesSize() {
    $('.incomplete img').width($('#piece_1 img').width()+'px');
}

function clickCard(el) {
    if (!timer_started) { //inicio o cronometro se ainda nao foi iniciado
        startTimer();
        timer_started = true;
    }

    el = $(el);
    var img = el.data('img');
    var id = $(el).attr('id');

    el.attr('src', img); //exibe a peça 
    
    if (clicked.id == 0) { // se for primeira peça a ser virada, seto os dados
        clicked.id = id; 
        clicked.img = img;
    } else {
        increaseMoves();

        $('.unclicked').attr('onclick','javascript:void(0)');

        if (!(clicked.img == img && clicked.id != id)) { //se não for a mesma img em uma peça diferente, não faz nada
            // espero dois segundos e desviro as duas peças
            setTimeout(function(){
                $('#'+clicked.id).attr('src', 'img/back.png');
                $('#'+id).attr('src', 'img/back.png');

                clicked = {'id': 0, 'img': ''}; // zero os dados de peça clicada
                $('.unclicked').attr('onclick', 'clickCard(this)');
            }, 2000);
        } else {
            increaseHits();

            $('#'+clicked.id).attr('onclick','javascript:void(0)').removeClass('unclicked'); //tiro o clique das peças iguais
            $('#'+id).attr('onclick','javascript:void(0)').removeClass('unclicked');

            if ($('.unclicked').length == 0) { // caso todas as peças estejam desviradas, paro o cronômetro
                stopTimer();
                $('#replayModal').modal('show');
            }

            clicked = {'id': 0, 'img': ''}; // zero os dados de peça clicada
            $('.unclicked').attr('onclick', 'clickCard(this)');
        }
    }
}

function startTimer() {
    interval_id = setInterval(function(){
        var seconds = parseInt($('#sec').text());
        var minutes = parseInt($('#min').text());
        var hours = parseInt($('#hour').text());

        seconds = seconds + 1;

        if (seconds > 59) {
            seconds = 0;
            minutes = minutes + 1;

            if (minutes > 59) {
                minutes = 0;
                hours = hours + 1;
            }
        }

        $('#sec').text(seconds);
        $('#min').text(minutes);
        $('#hour').text(hours);
    }, 1000);
}

function stopTimer() {
    clearInterval(interval_id);
}

function increaseMoves() {
    var moves = parseInt($('#moves').text());
    $('#moves').text(moves+1);
}

function increaseHits() {
    var hits = parseInt($('#hits').text());
    $('#hits').text(hits+1);
}